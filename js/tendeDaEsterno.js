var json = null;
function loadJson(callback) {
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", "data.json", true);
  xobj.onreadystatechange = function () {
    if (xobj.readyState == 4 && xobj.status == "200") {
      callback(xobj.responseText);
    }
  };
  xobj.send(null);
}

function init(tipologia) {
  loadJson(function (response) {
    json = JSON.parse(response);
    console.log(tipologia);
    var containerName = tipologia + "Container";
    var containerDiv = document.getElementById(containerName);
    containerDiv.id = "containerDiv";

    var num = json.tendeDaEsterno[0][tipologia].length;

    var j = 0;
    var k = 0;
    var h = 4;
    while (j < num) {
      var row = document.createElement("div");
      containerDiv.appendChild(row);

      for (var i = k; i < h; i++) {
        var col = document.createElement("div");
        var img = document.createElement("img");
        var nameDiv = document.createElement("div");
        var name = document.createElement("div");
        var btn = document.createElement("button");
        var modal = document.getElementById("modalBody");
        var modalTitle = document.getElementById("exampleModalLongTitle");
        var modalFooter = document.getElementById("modalFooter");
        btn.style = "padding: 0; border:white; background: none";
        btn.setAttribute("data-toggle", "modal");
        btn.setAttribute("data-target", "#exampleModalCenter");
        row.className = "row mt-5 justify-content-md-center";
        col.className = "col-6 col-sm-6 col-md-3 col-lg-3 zoom";
        col.id = "col";

        img.width = "150";
        img.height = "150";
        img.className = "d-bloc w-100 h-60 ";
        nameDiv.style.marginTop = "10px";
        name.style.textAlign = "center";

        row.appendChild(col);
        if (json.tendeDaEsterno[0][tipologia][i] != undefined) {
          img.src =
            "img/tendeEsterno/" +
            tipologia +
            "/" +
            json.tendeDaEsterno[0][tipologia][i] +
            ".png";
          img.alt =
            "img/tendeEsterno/" +
            tipologia +
            "/" +
            json.tendeDaEsterno[0][tipologia][i] +
            ".png";

          btn.id = json.tendeDaEsterno[0][tipologia][i];
          btn.onclick = function () {
            modal.innerHTML =
              "<img src = 'img/tendeEsterno/" +
              tipologia +
              "/" +
              this.id +
              ".png' width='100%' alt='img/tendeEsterno/" +
              tipologia +
              "/" +
              this.id +
              ".png'>";

            modalTitle.innerHTML =
              "<b style='color:black; text-transform: uppercase';>" +
              this.id.replaceAll("_", " ") +
              "</b>";
            var description = "descrizione" + tipologia;
            var numItems = json.tendeDaEsterno[0][tipologia].length;
            console.log(description);
            var position;
            for (var y = 0; y < numItems; y++) {
              if (this.id == json.tendeDaEsterno[0][tipologia][y]) {
                position = y;
              }
            }

            modalFooter.innerHTML =
              "<i><small>" +
              json.tendeDaEsterno[0][description][position] +
              "</small></i>";
          };

          var nomeTenda = json.tendeDaEsterno[0][tipologia][i];
          nomeTenda = nomeTenda.replaceAll("_", " ");
          name.innerHTML =
            "<b style='color:#808080; text-transform: capitalize';><big>" +
            nomeTenda +
            "</big></b>";
          btn.appendChild(img);
          col.appendChild(btn);
          col.appendChild(nameDiv);
          nameDiv.appendChild(name);
        }
      }

      k = k + 4;
      h = h + 4;
      j = j + 4;
    }
  });
}

init("tendeABracci");
init("cappottine");

$(function () {
  $("#nav-placeholder").load("navbar.html");
  $("#footer-placeholder").load("footer.html");
});
