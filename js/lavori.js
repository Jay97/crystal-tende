document.addEventListener("DOMContentLoaded", function () {
  var secondarySlider = new Splide("#secondary-slider", {
    fixedWidth: 100,
    rewind: true,
    height: 60,
    cover: true,
    gap: 10,
    pagination: false,
    arrows: false,
    isNavigation: true,
    focus: "center",
    breakpoints: {
      600: {
        fixedWidth: 66,
        height: 40,
      },
    },
  }).mount();

  var primarySlider = new Splide("#primary-slider", {
    type: "fade",
    heightRatio: 0.4,
    pagination: true,
    isNavigation: true,
    arrows: true,
    cover: true,
    breakpoints: {
      600: {
        height: 340,
      },
    },
  });

  primarySlider.sync(secondarySlider).mount();
});
